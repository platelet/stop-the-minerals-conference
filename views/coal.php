<!-- Main -->
<div id="main">
    <article class="post featured">
      <h3><strong>Why are we protesting the minerals forum? Why is coal worth protesting?</strong></h3>
      <p>This mineral forum aims to create a platform for businesses that are planning expand coal mining in Aotearoa, especially the exploitation of the west coast.</p>

      <h3><strong>How much coal does New Zealand use and what is it used for?</strong></h3>
      <span class="image fit"><img src="https://s3.amazonaws.com/stmf/assets/coal_amount.png" alt="Graph showing amount of coal used in New Zealand" /><small><i>figure.nz (May 2019)</i></small></span>
      <p style="min-height: 340px;"><span class="image left"><img src="https://s3.amazonaws.com/stmf/assets/coal_use.png" alt="Pie graph showing use in New Zealand by sector/use" /><small><i>letstalkaboutcoal.co.nz (May 2019)</i></small></span> In 2017, we used 1,144,392.30 kilotons of coal. To put this in perspective, complete combustion of 1 ton of this coal will generate about 2.86 tons of carbon dioxide (Hong & Slatick. 1994). This means that in 2017 alone, we released approximately 3,272,961.978 kilotons of CO2 into our atmosphere.</p>

      <h3><strong>Why does it matter?</strong></h3>
      <p>Coal mining and coal burning have a plethora of negative effects on human health, the environment, and the economy.<br>The most common argument against the use of coal is that it contributes to climate change. This is because coal produces the most CO2 emissions/Kg out of any other fossil fuel. The longest lasting GHG (green-house gas) emissions are also associated with coal.</p>

      <h3><strong>What is climate change?</strong></h3>
      <p>Climate change is when greenhouse gasses (Such as carbon dioxide and methane) are produced by human activities. These greenhouse gasses cause the atmosphere to thicken and absorb more heat from the sun. This effect is responsible for warming the earth's atmosphere and oceans. </p>
      <p>By producing 3.72 million kilotons of CO2, New Zealand’s coal combustion is exacerbating ocean acidification (which is responsible for killing coral reefs) and sea level rise. </p>
      <p>Coal combustion in high quantities also creates acid rain. Coal combustion produces sulphur oxides and nitric oxides. The sulphur oxides and nitric oxides react with water vapor and other chemicals high in the atmosphere to form sulfuric and nitric acids. The acids formed usually dissolve in the suspended water droplets in clouds or fog. Acid rain causes environmental degradation as well as the destruction of historic buildings. Coal combustion also releases mercury into the environment, causing further environmental degradation.</p>
      <p>Coal mining also has major environmental ramifications. The mining process can poison groundwater, and toxic waste dumps pose an equally challenging problem. Waste dumps generated from mining that exposes sulfur-bearing overburden can be active sources of acid generation with the potential to severely contaminate soils, surface and groundwater, and endanger both local and downstream ecosystems. Coal fires produce an enormous amount of toxic gases and particulate matter, that over many years, contribute significantly to the global destruction of the environment and the health of its inhabitants.</p>
      <p>Coal use has been linked to sever health effects. China’s coal use has contributed to an increase in lung cancer of 464·8% from 1974-2005 and respiratory infections. There’s also the risk of fatal mine accidents, and a severely reduced life expectancy. One study in China in 1997 found that the life expectancy for miners was between 50-60 years.</p>
      <p>Coal is economically unstable. Coal, like all fossil fuels, is a finite resource. The 2016 IPCC report says that the possibility for resources to cross over to reserves is expected to be limited since coal reserves are likely to last around 100 years at current rates of production. It cannot be expected to last forever. Oil was New Zealand's fourth biggest export earner in 2010. In 2016, it slipped to 19th, earning $561 million in the year to July. Coal value fluctuates heavily due to the availability of other natural resources. Examples of this include low natural gas prices in the United States due to shale gas production making existing coal plants uneconomic to run.</p>

      <h3><strong>Are there any benefits to coal mining?</strong></h3>
      <p>Right now, its cheap</p>

      <section style="text-align: left; font-size: 9pt; padding-top: 40px;">
        <small>References:</small>
        <ul>
          <li>B. Hong and E. Slatick. 1994. Carbon Dioxide Emission Factors for Coal. Energy Information Administration: Quarterly Coal Report. https://www.eia.gov/coal/production/quarterly/co2_article/co2.html</li>
          <li>NZ’s coal usage breakdown: https://www.letstalkaboutcoal.co.nz/coal-in-nz/how-nz-uses-coal/</li>
          <li>New Zealand’s Coal usage over time graph: https://figure.nz/chart/VJk3hKPj2IWNpEz0</li>
          <li>China tackles the health effects of air pollution. Zhu Chen. Jin-Nan Wang. Guo-Xia Ma. Yan-Shen Zhangc. The lancet. Volume 382, Issue 9909, 14–20 December 2013, Pages 1959-1960.</li>
          <li>Gaseous Pollutants Formation and Their Harmful Effects on Health and Environment Yousef S. H. Najjar. Innovative Energy Policies, 2011, Vol.1, pp.1-9. https://www.researchgate.net/profile/Yousef_Najjar/publication/228901085_Gaseous_Pollutants_Formation_and_Their_Harmful_Effects_on_Health_and_Environment/links/00b7d52780fe3b2fb8000000/Gaseous-Pollutants-Formation-and-Their-Harmful-Effects-on-Health-and-Environment.pdf</li>
          <li>Air Pollution Threatens the Health of Children in China. Alexander Millman, BAa, Deliang Tang, DrPHb, Frederica P. Perera, DrPHb a Department of Medical Education, Mount Sinai School of Medicine, New York, New York; bColumbia Center for Children’s Environmental Health, Mailman School of Public Health, Columbia University, New York, New York. https://pediatrics-aappublications-org.ezproxy.otago.ac.nz/content/pediatrics/122/3/620.full.pdf</li>
          <li>Coal fires burning out of control around the world: thermodynamic recipe for environmental catastrophe. Stracher, Glenn B ; Taylor, Tammy P. International Journal of Coal Geology, 2004, Vol.59(1), pp.7-17</li>
          <li>Climate change and energy policies, coal and coalmine methane in China. Yang, Ming. Energy Policy, 2009, Vol.37(8), pp.2858-2869</li>
          <li>Bruckner T., I.A. Bashmakov, Y. Mulugetta, H. Chum, A. de la Vega Navarro, J. Edmonds, A. Faaij, B. Fungtammasan, A. Garg, E. Hertwich, D. Honnery, D. Infield, M. Kainuma, S. Khennas, S. Kim, H.B. Nimir, K. Riahi, N. Strachan, R. Wiser, and X. Zhang, 2014: Energy Systems. In: Climate Change 2014: Mitigation of Climate Change. Contribution of Working Group III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Edenhofer, O., R. Pichs-Madruga, Y. Sokona, E. Farahani, S. Kadner, K. Seyboth, A. Adler, I. Baum, S. Brunner, P. Eickemeier, B. Kriemann, J. Savolainen, S. Schlömer, C. von Stechow, T. Zwickel and J.C. Minx (eds.)]. Cambridge University Press, Cambridge, United Kingdom and New York, NY, USA. https://www.ipcc.ch/site/assets/uploads/2018/02/ipcc_wg3_ar5_chapter7.pdf</li>
          <li>Adverse effects of coal mine waste dumps on the environment and their management.(Report) Adibee, N.; Osanloo, M.; Rahmanpour, M. Environmental Earth Sciences, Oct, 2013, Vol.70(4), p.1581(12)</li>
          <li>Mining drags on growing economy. RNZ. 2016. https://www.rnz.co.nz/news/business/313650/mining-drags-on-growing-economy</li>
          <li>Intergovernmental Panel on Climate Change (IPCC). Guidelines for National Greenhouse Gas Inventories (2006).</li>
          <li>Environmental issues from coal mining and their solutions. Bian, Zhengfu; Inyang, Hilary I; Daniels, John L; Otto, Frank; Struthers, Sue. Mining Science and Technology (China), March 2010, Vol.20(2), pp.215-223.</li>
          <li>Expectancy of working life of mine workers in hunan province. Sun, Z.Q. ; Zhang, Y.R. ; He, T. ; Yang, C.G. Public Health, 1997, Vol.111(2), pp.81-83.</li>
        </ul>
      </section>
    </article>
</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
