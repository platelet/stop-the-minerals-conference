<!-- Intro -->
<?php if ($page === 'home') { ?>
<div id="intro">
  <h1>Stop the minerals forum!</h1>
  <h2 style="font-size: 3.5em;">BLOCKADE THE MINERALS CONFERENCE!</h2>
  <h2>May 27th - 28th, Dunedin, 2019</h2>
  <p>Stand up for climate justice and get in the way of mineral ‘business as usual’</p>
  <ul class="actions">
    <li><a href="#header" class="button icon solo fa-arrow-down scrolly">Continue</a></li>
  </ul>
</div>
<?php } ?>

<!-- Header -->
<header id="header">
  <?php if ($page === 'home') { ?>
    <a href="/" class="logo">Stop the minerals forum</a>
  <?php } else { ?>
    <a href="/" class="logo">Stop the minerals forum</a>
  <?php } ?>
</header>

<!-- Nav -->
<nav id="nav">
  <ul class="links">
    <li <?php if ($page === 'home') echo 'class="active"'; ?>><a href="/">Home</a></li>
    <li <?php if ($page === 'foulden') echo 'class="active"'; ?>><a href="/foulden_maar">Foulden Maar</a></li>
    <li <?php if ($page === 'coal') echo 'class="active"'; ?>><a href="/coal">Coal!</a></li>
    <li <?php if ($page === 'help') echo 'class="active"'; ?>><a href="/help">Help</a></li>
    <li <?php if ($page === 'safer') echo 'class="active"'; ?>><a href="/safer_spaces">Safer Spaces</a></li>
    <li <?php if ($page === 'register') echo 'class="active"'; ?>><a href="/register">Register</a></li>
  </ul>
  <ul class="icons">
    <li><a href="https://www.facebook.com/STOP-The-Minerals-Forum-COALition-313455869325411/" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
  </ul>
</nav>
