<!-- Main -->
<div id="main">

  <!-- Featured Post -->
    <article class="post featured">

      <p>On 27-29 May 2019 the mineral and coal industry is holding its annual business conference. Government ministers are lobbied, businesses network and oil and gas exploration permits are announced.</p>
      <p>Luckily Oil Free Wellington is holding a rally and blockade and disrupt this conference.</p>

      <p><strong>What's the problem?</strong></p>
      <p>At a time of climate crisis we need to be ending fossil fuel exploration, not expanding and continuing it. In order to prevent catastrophic climate change we cannot burn all of the coal, oil and gas which we've already found let alone explore for more.</p>

      <p><strong>Do we have oil drilling in Aotearoa?</strong></p>
      <p>Over 1200 wells have been drilled for oil and gas across Aotearoa in the last 150 years. Almost 1000 of these are onshore and mostly in Taranaki which has been producing oil/gas since 1969. As the older reserves deplete, more wells are drilled and aggressive techniques notably fracking and horizontal drilling are employed to access previously unobtainable reserves or to extend the lifespan of old fields, threatening the health of local communities and the environment.<br/>Exploration for offshore deep sea oil has grown rapidly. Tens of thousands of square kilometres have been opened to exploration by the then National government despite strong public opposition.<br/>Oil companies Petrobras and Anadarko faced flotillas, kayaks and swimmers in 2011 and 2014. Local communities mobilized in their thousands the length of both islands with beach protests against Anadarko's exploratory drilling.<br/>In 2014 and 15 Te Tai Tokerau strongly resisted Norwegian oil company Statoil.  The seismic blasting ship Amazon Warrior  was contracted by Statoil and Chevron to survey off the East Coast in 2016 and 17. It was intercepted by Te Matau a Māui, a double-hulled waka organized by Te Ikaroa. And people from the Greenpeace boat Taitu literally jumped into the water to disrupt exploration (and are currently facing charges under the Anadarko Amendment which criminalised protest at sea).<br/>In December 2017, Taranaki locals and supporting groups from across Aotearoa <a href="http://www.climatejusticetaranaki.info/" target="_blank">rallied</a> on the water to stop the Amazon Warrior contracted by Schlumberger to look for oil off the west coast of the North Island.<br/>On land, local iwi and others have been at the forefront resisting the expansion of oil and gas exploration that fracking has allowed. Permits on the East Coast and Tararua have been relinquished - while Southland is gearing up to stop any exploration.</p>

      <p><strong>What's the solution?</strong></p>
      <p>We need a movement for climate justice. One which acknowledges that the current climate crisis is interconnected with issues such as war and poverty, problems caused by structural systems like capitalism, colonisation and patriarchy.<br/>Those who will be most effected by climate change are those already suffering under our present system and those who contributed the least to climate change. Refugees, the working class, women, children, people of colour and Indigenous peoples, will all be worst effected by climate change.<br/>We need a movement which works with groups like unions and iwi, to create a future which is just and sustainable.</p>

      <p><strong>What is Climate Justice?</strong></p>
      <p>Climate justice means an acknowledgement that climate change is a product of socio-economic and political systems, like capitalism, patriarchy and colonialism.<br/>Therefore, it can’t be adequately addressed through individual action (e.g. green consumption) or technology.<br/>Climate justice begins from a recognition that those most affected by climate change are the least responsible for it.<br/>For instance, Indigenous communities and people of colour, women, the working class and economically marginalised, and children are most likely to bear the brunt of a changing climate. We are in solidarity with our Pacific neighbours who are already experiencing the effects of, and resisting, climate change.<br/>Climate justice action seeks to end systems of oppression and prioritise solutions envisioned by communities that are most affected by change, and the costs of these solutions are borne equitably.</p>

      <p><strong>Don't we have a new government?</strong></p>
      <p>The new Labour government has talked big on tackling climate change but it is still maintaining oil and gas exploration.<br/>Jacinda Ardern has said that climate change is our generation's "nuclear free moment" but she has also refused to revoke oil and gas exploration permits, which cover thousands of square kilometres.<br/>Now is the time to put pressure on the industry and the government. We can make it clear that their fossil fuel agenda will face staunch resistance.</p>

      <p><strong>How can I help?</strong></p>
      <p>Get in touch with Oil Free Wellington at <a href="mailto:oilfreewellington@gmail.com">oilfreewellington@gmail.com</a></p>
      <p>Ask to be put on our mailing list and receive updates on the conference campaign and oil and gas in Aotearoa.</p>

      <a href="#" class="image main"><img src="images/system_change.jpg" alt="" /></a>
      <!-- <ul class="actions">
        <li><a href="#" class="button big">Join the group</a></li>
        <li><a href="#" class="button big">Pledge to Act</a></li>
        <li><a href="#" class="button big">Save the Date</a></li>
      </ul> -->
    </article>

</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <h3>Groups commited to this kaupapa and action</h3>
    <div style="display: inline-block; margin: 5px;">
      <img src="images/ofw.png" alt="" width="100">
      <p>Oil Free Wellington</p>
    </div>
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
