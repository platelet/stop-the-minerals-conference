<!-- Main -->
<div id="main">

  <!-- Featured Post -->
    <article class="post featured">

      <p><strong>What can you do now?</strong></p>
      <ul style="text-align: left;">
        <li>Rally your crew, book the time of work, make travel plans. We will do our best to facilitate carpooling to Ōtepoti, to set you up with accomodation and to make sure you are supported before, during and after the action.</li>
        <li>Let us know if you can help in any way in the next weeks. We need support with media, fundraising, accommodation, transport, creative elements (placards, noise making devices) and more. Email us at: <a href="mailto:stopmineralsforum@gmail.com">stopmineralsforum@gmail.com</a></li>
        <li>Help us <a href="https://givealittle.co.nz/cause/stop-the-minerals-forum" target="_blank">raise funds</a> to support this resistance</li>
      </ul>
    </article>

</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
