<!-- Main -->
<div id="main">

  <!-- Featured Post -->
    <article class="post featured">
      <header class="major">
        <h2>JOIN US TO STAND AGAINST COAL</h2>
      </header>
      <p>Nau mai, Hāere mai, tautoko mai</p>
      <br>

      <p>The <a href="https://www.mineralsforum.co.nz/" target="_blank">NZ Minerals Forum</a> (ie Coal Conference) is coming to Dunedin 27-29 May to promote coal, and other polluting industries, expansion in Aotearoa NZ. More coal, more profit, more destruction of our future. We are in a climate emergency. Coal does not belong in our future. People around the world are saying “Enough” to coal and other polluting industries. And so are we.</p>
      <p>The Stop the Minerals Forum coalition is working together to organise a strong and non-violent opposition to the 2019 NZ Minerals Forum. Come join us on 27-29th May at the Dunedin Centre to show the coal industry that business as usual is over!</p>

      <br>

      <h2 class="align-left">What will the action will look like?</h2>
      <p>We are organising to peacefully resist the coal industry by using people power to delay delegates entering the Dunedin Centre and cause as much disruption as possible. There will be something for everyone on the day with different levels of engagement- from singing to more direct action- and plenty of other roles and activities.</p>

      <h2 class="align-left">Book the day off! 28th May</h2>
      <p>The main day we are asking people to take action is the first full day of the forum, Tuesday 28 May, although there will be other events happening in the lead-up to, and during the forum- so watch this space! There will be a Group Briefing the evening before the action (27th May) so that everyone is up to date with plans.</p>
      <p>You can also join the <a href="https://facebook.com">Facebook event</a> for updates.</p>

      <h2 class="align-left">Who can get involved?</h2>
      <p>As many people as we can muster! The climate justice movement is strong and growing in Ōtepoti and we are motivated to use the forum as a stepping stone to bigger and better things (including protecting our waters from OMV)!  The more people that come along the more impact we will have, all are welcome!</p>

      <h2 class="align-left">What can you do now?</h2>
      <p>Register for the action <a href="/register">here</a>, join the Facebook <a href="https://www.facebook.com/STOP-The-Minerals-Forum-COALition-313455869325411/">page</a> and <a href="https://facebook.com">event</a>, share it with your friends, get in <a href="https://www.facebook.com/STOP-The-Minerals-Forum-COALition-313455869325411/">contact with us</a>, Rally your crew, book the time of work, make travel plans.</p>
      <p>Ask us if you want carpooling to Ōtepoti; set you up with accommodation; and to make sure you are supported before, during, and after the action.</p>

      <h2 class="align-left">Can you chip in?</h2>
      <p><a href="https://www.facebook.com/STOP-The-Minerals-Forum-COALition-313455869325411/">Let us know</a> if you can help in any way in the next 5 weeks.</p>
      <p>We need support with media, fundraising, accommodation, transport, creative elements (placards, noise making devices) and more…</p>

      <button type="button" name="button">
        <a href="/register">Reigster Now!</a>
      </button>

      <!-- <ul class="actions">
        <li><a href="#" class="button big">Join the group</a></li>
        <li><a href="#" class="button big">Pledge to Act</a></li>
        <li><a href="#" class="button big">Save the Date</a></li>
      </ul> -->
    </article>

</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
