<!-- Main -->
<div id="main">
    <article class="post featured">
      <header class="major">
        <h2>MINING COMPANY PLANS TO DESTROY CLIMATE CHANGE RECORD</h2>
      </header>

      <span class="image fit">
        <img src="https://s3.amazonaws.com/stmf/assets/areial.jpeg" alt="Aerial shot of foulden maar volcanic crater">
      </span>
      <p>Foulden Maar is a 23 million year old volcanic crater near Middlemarch, Otago. No other known site in the world contains such high resolution climate change records spanning the last period of major deglaciation of Antarctica; information that is increasingly important as we deal with the impacts of climate change. Foulden Maar is also one of the most significant palaeontological (fossil) sites in the Southern Hemisphere.</p>
      <span class="image fit">
        <img src="https://s3.amazonaws.com/stmf/assets/fossil.jpeg" alt="Fossilised fish">
      </span>
      <p>Transnational mining company Plaman Resources plan to destroy all this and process the diatomite into a fertiliser for the palm oil plantations of its major shareholder, Malaysian Iris Corporation, and as a stockfeed additive for offshore factory farms and feedlots. To process the diatomite, it is highly likely Plaman will be using coal.</p>
      <p>Plaman have been signing back room deals with local businesses and councils, and have <i>“letters of support”</i> from the <i>“pro-mining”</i> Dunedin City Council. They have employed former Labour MP Clayton Cosgrove with his <i>“outstanding relationships with the ruling Labour Government”</i> to ease their way through approval and compliance <i>“as soon as possible”</i>. Following a leaked Goldman Sacks report, Plaman have withdrawn from the Minerals Forum; a tactical retreat, while they alter their window dressing with a corporate restructure. They will no doubt have an informal presence at the Forum.</p>
    </article>
</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
