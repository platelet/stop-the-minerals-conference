<!DOCTYPE HTML>
<html>
	<head>
		<title>Stop the mineral conference | <?php echo $title ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>

	</head>
	<body class="is-loading">

		<!-- Wrapper -->
			<div id="wrapper" class="fade-in">

				<!-- HEADER CONTENT ## $header_content -->
        <?php echo $header_content; ?>

        <!-- BODY CONTENT ## $body_content -->
				<?php echo $body_content; ?>

				<div id="page_buffer"></div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

			<script src="//static.getclicky.com/js" type="text/javascript"></script>
			<script type="text/javascript">try{ clicky.init(101090844); }catch(e){}</script>
			<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/101090844ns.gif" /></p></noscript>

	</body>
</html>
