<!-- Main -->
<div id="main">
  <article class="post featured policy">
    <header class="major">
      <h2>Safer Spaces Policy</h2>
    </header>

    <p><a href="https://s3.amazonaws.com/stmf/Stop-the-Minerals-Forum-Safe-Space-Policy.pdf" target="_blank">link to download as pdf</a></p>

    <p>This document sets out expectations about how people will treat each other at Stop the Minerals Forum events, spaces and forums.</p>

    <p><b><i>Background for policy:</i></b></p>
    <ul>
      <li>The Stop the Minerals Forum Coalition are committed to providing a safe and harassment-free experience for everyone, regardless of gender, gender identity/expression, sexual orientation, ability, physical appearance, size, ethnicity, or religion. This applies to all meetings, events, demonstrations and online platforms hosted by the coalition.</li>
      <li>We will not tolerate harassment at any event or on any platform hosted by Stop the Minerals Forum Coalition. We will endeavour to have organisers and volunteers available at all times to ensure our events are as safe and accessible as possible.</li>
      <li><b>Being non-racist, non-sexist, non-homophobic etc is not enough. We believe we must be actively anti all forms of oppression.</b></li>
      <li>While we cannot guarantee such an environment, we hope that this policy will help people to feel comfortable to approach us with any concerns and to understand what their role is in creating a safer space.</li>
    </ul>

    <p><b><i>Guidelines for all participants:</i></b></p>
    <ul>
      <li>Contribute to making all events hosted by Stop the Minerals Forum enjoyable and inclusive. Look out for each other. Respect the physical, emotional, and mental boundaries and the safety of our fellow  activists. </li>
      <li>As participants at our events, we ask that you take responsibility for your behaviour and understand the ways in which it can affect others. We value freedom of expression, but not at the cost of alienating or harming others.</li>
      <li>People who are unwilling to discuss and correct their harmful behaviour will be immediately excluded from the event with no exceptions.</li>
      <li>We prioritise the needs and feelings of people who may belong to marginalized groups or who may have been harmed in the past, this means at times people may be excluded from spaces for past actions or harmful rhetoric, until a resolution can be reached.  An example of this is that someone who is known to have expressed racist views may be excluded until they take steps to correct the harm</li>
    </ul>

    <p><b><i>Harmful Behaviour Includes but is not limited to:</i></b></p>
    <ul>
      <li>Making racist, sexist, classist, transphobic, homophobic, ableist, ageist or body-shaming comments of any kind</li>
      <li>Catcalling or sexual harassment</li>
      <li>Failure to respect the physical and/or emotional safety of others</li>
      <li>Failure to read body language and consequently invading the personal space of others</li>
      <li>Cultural appropriation</li>
      <li>Being too drunk / high to monitor your behaviour and its impact on others</li>
      <li>Generally disrespecting other attendees rights to participate and have a safe and enjoyable time at our events.</li>
    </ul>

    <p><b><i>Be responsible for those around you:</i></b></p>
    <ul>
      <li>Whether or not it directly affects you, we encourage participants to engage with people behaving in an inappropriate way if this can be done safely. If you do feel comfortable speaking up when someone is inappropriate, we ask that you refrain from confrontational behaviour that may spark aggression. It is more effective to quietly talk to the person and point out that they are making someone uncomfortable, rather than publicly humiliating them or using physical force.</li>
      <li>Sometimes it could be as simple as saying: ‘I think women are just as able to fix a bike as men are’ or ‘that sounded a bit racist, can you explain what you meant?’ If a person is invading another’s personal space and failing to read body language, you could say “I don’t think they’re cool with that, would you mind giving them some space?”</li>
    </ul>

    <p><b><i>Safe Space Team (On an event - by - event basis)</i></b></p>
    <ul>
      <li>A team of volunteers will be visible and available throughout  all Stop the Minerals Forum events to talk to participants about any problematic behaviour or situation and to offer support. <b>Phone numbers of the Core Team members will be visible should you need to contact them by phone.</b></li>
    </ul>

    <p><b><i>The tuff stuff...</i></b></p>
    <ul>
      <li>Stop the Minerals Forum Safe Space Volunteers reserve the right to ask participants who have made others feel uncomfortable, threatened, or unsafe to change or address their behaviour or language. If the person is unwilling or unable to do so they will be asked to leave any event or platform we are hosting.</li>
    </ul>

    <p><b><i>Additional Guidelines for attending Stop the Minerals Forum Events:</i></b></p>
    <ul>
      <li><b>Weapons:</b> No weapon may be brought or used on the various sites of our actions, obvious exceptions to this are items which have cultural, religious or symbolic significance eg. Taiaha.</li>
      <li><b>Drugs and alcohol:</b> Individuals present on the site of our actions will not consume alcohol or mood-altering drugs.</li>
      <li><b>Physical presentation:</b> We ask that people present themselves in a way that is not deliberately meant to be intimidating.</li>
      <li><b>Violence:</b> During our actions, people present on site will Abstain from physical and verbal violence to persons / animals / the environment, including insults towards our members and participants at our events.. Our attitude will be one of openness towards anyone we encounter.</li>
      <li><b>Police:</b> If possible try to minimise any interactions with police,  it can bring extra unwanted danger and attention to group members and participants. If they try to talk to you in relation to Stop the Minerals Forum activities or give you instructions let the group know.</li>
      <li>Please be aware of power at all times.</li>
      <li>Abusive situations are often created when people are unaware of the power they have in a relationship or situation.</li>
      <li>Gender, sexuality, size, physical impairment, ethnicity, age, class, education, mental health, who you know and how well you know them can all affect how much power a person has in any situation.</li>
      <li>Don’t assume everyone feels as comfortable as you do, or is completely able to inform you if you are saying and doing hurtful things.</li>
      <li>We understand that everyone makes mistakes but hurting others isn’t ok.</li>
      <li>We hope that this will give an idea of some ways to make things right and take responsibility for mistakes which have hurt people.</li>
    </ul>

    <p>If any of Stop the Minerals Forum volunteers make others feel unsafe or hurt, please contact us at (environmentaljusticeotepoti@gmail.com).</p>
    <p>Feedback or complaints against us are welcome and will be dealt with in a reasoned, fair, and respectful way which values all feedback and puts the safety and comfort of our participants first.</p>

    <hr>
    <p>&lt3</p>
    <p>Thank you for taking the time to read through our Safer Spaces Policy. We can only make our spaces safe with your support. We are lucky to have such a positive and loving community, and we are glad that we can do our bit to help to create safer spaces. We believe this policy is an important and valuable way to address some of the issues that people can experience when attending public events. If you have feedback, please feel free to contact us at: environmentaljusticeotepoti@gmail.com</p>

  </article>
</div>
