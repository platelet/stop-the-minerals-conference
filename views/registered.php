<!-- Main -->
<div id="main">
  <section>
    <h2>Registered!</h2>
    <p>Thank you for registering to take action. You will recieve an email shortly with information, and we will be in touch again closer to the action</p>
    <p>If you have any ideas, questions or want to help please <a href="mailto:oilfreewellington@gmail.com">get in touch!</a></p>
  </section>
</div>
