<!-- Main -->
<div id="main">
  <section>
    <h2>Register</h2>
    <form class="alt" method="post" action="/register">
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="name">Name</label>
          <input type="text" name="name" id="name" />
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="email">Email</label>
          <input type="text" name="email" id="email" />
        </div>
      </div>
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="phone">Phone</label>
          <input type="text" name="phone" id="phone" />
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="region">Region</label>
          <input type="text" name="region" id="region" />
        </div>
      </div>

      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <input type="radio" id="accom_pro" name="accom" value="can_provide">
          <label for="accom_pro">I can provide accommodation in Dunedin</label>
        </div>
        <div class="6u 12u$(xsmall)">
          <input type="radio" id="accom_need" name="accom" value="needs_accom">
          <label for="accom_need">I need accommodation in Dunedin</label>
        </div>
      </div>

      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <input type="radio" id="trans_pro" name="trans" value="can_provide">
          <label for="trans_pro">I can provide transport to Dunedin</label>
        </div>
        <div class="6u 12u$(xsmall)">
          <input type="radio" id="trans_need" name="trans" value="needs_trans">
          <label for="trans_need">I need transport to Dunedin</label>
        </div>
      </div>
      <div class="row uniform">
        <ul class="actions">
          <li><input type="submit" value="Submit" /></li>
        </ul>
      </div>
      <small>Your data will be held securely, and deleted after the action</small>
    </form>
  </section>
</div>
