<?php

  require 'flight/Flight.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/SMTP.php';

  error_reporting(0);

  //restart any current browser sessions
	session_start();
  if (!$_SESSION['user_defined_admin_in']) {
    $_SESSION['user_defined_admin_in'] = 'NO';
  }

  define('DB_USER'      , getenv('DATABASE_USER'));
	define('DB_PASS'      , getenv('DATABASE_PASSWORD'));
	define('DB_NAME'      , getenv('DATABASE_NAME'));
	define('DB_HOST'      , getenv('DATABASE_HOST'));
  define('ADMIN_PASS'   , getenv('ADMIN_PASSWORD'));
  define('EMAIL_PASS'   , getenv('EMAIL_PASSWORD'));
  define('EMAIL_ADDRESS', getenv('EMAIL_ADDRESS'));

	//connect to MySQL
  $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  if (mysqli_connect_errno($mysqli)) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

  Flight::set('flight.views.path', 'views/');

  Flight::route('/.well-known/acme-challenge/@challengeHash', function($challengeHash) {
    echo $challengeHash.'.tvZoTWuQgfiNyjL60B8YO3MBL4hJ0Kt6-rCTO_Pi5AQ';
  });

  Flight::route('/', function(){
    Flight::render('header', array('page' => 'home'), 'header_content');
    Flight::render('home', array(), 'body_content');
    Flight::render('layout', array('title' => 'Stop the Minerals Conference'));
  });

  Flight::route('/about', function(){
    Flight::render('header', array('page' => 'about'), 'header_content');
    Flight::render('about', array(), 'body_content');
    Flight::render('layout', array('title' => 'About'));
  });

  Flight::route('/coal', function(){
    Flight::render('header', array('page' => 'coal'), 'header_content');
    Flight::render('coal', array(), 'body_content');
    Flight::render('layout', array('title' => 'About Coal'));
  });

  Flight::route('/foulden_maar', function(){
    Flight::render('header', array('page' => 'foulden'), 'header_content');
    Flight::render('foulden', array(), 'body_content');
    Flight::render('layout', array('title' => 'Foulden Maar'));
  });

  Flight::route('/help', function(){
    Flight::render('header', array('page' => 'help'), 'header_content');
    Flight::render('help', array(), 'body_content');
    Flight::render('layout', array('title' => 'How to Help'));
  });

  Flight::route('/principles', function () {
    Flight::render('header', array('page' => 'principles'), 'header_content');
    Flight::render('principles', array(), 'body_content');
    Flight::render('layout', array('title' => 'Principles'));
  });

  Flight::route('/safer_spaces', function () {
    Flight::render('header', array('page' => 'safer'), 'header_content');
    Flight::render('safer_spaces', array(), 'body_content');
    Flight::render('layout', array('title' => 'Safer Spaces Policy'));
  });

  Flight::route('/events', function () {
    Flight::render('header', array('page' => 'events'), 'header_content');
    Flight::render('events', array(), 'body_content');
    Flight::render('layout', array('title' => 'Week of Climate Justice'));
  });

  Flight::route('GET /register', function () {
    Flight::render('header', array('page' => 'register'), 'header_content');
    Flight::render('register', array(), 'body_content');
    Flight::render('layout', array('title' => 'Register'));
  });
  Flight::route('GET /registered', function () {
    Flight::render('header', array('page' => 'registered'), 'header_content');
    Flight::render('registered', array(), 'body_content');
    Flight::render('layout', array('title' => 'Registered'));
  });

  Flight::route('GET /manage', function () {
    global $mysqli;
    if ($_SESSION['user_defined_admin_in'] == 'confirmed') {
      $query = "SELECT name, email, phone, region, transport, accommodation, createdAt FROM registers WHERE 1";
      $res = mysqli_query($mysqli, $query);
      $data = [];
      $csv = 'name,email,phone,region,accommodation,transport,createdAt\n';
      while ($row = mysqli_fetch_assoc($res)) {
        $csv .= $row['name'].','.$row['email'].','.$row['phone'].','.$row['region'].','.$row['accommodation'].','.$row['transport'].','.$row['createdAt'].'\n';
        array_push($data, $row);
      }
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_view', array('data' => $data, 'csv' => $csv), 'body_content');
      Flight::render('layout', array('title' => 'Admin'));
    } else {
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_login', array(), 'body_content');
      Flight::render('layout', array('title' => 'Admin Login'));
    }
  });
  Flight::route('POST /manage', function () {
    $request = Flight::request();
    $form_data = $request->data;
    $password = $form_data->password;
    if ($password == ADMIN_PASS) {
      $_SESSION['user_defined_admin_in'] = 'confirmed';
      Flight::redirect('/manage');
    } else {
      die('permission denied');
    }
  });

  function sendEmail($name, $email) {
    $mail = new PHPMailer(true);
    try {
      //Server settings
      $mail->SMTPDebug = 2;
      $mail->isSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = EMAIL_ADDRESS;
      $mail->Password = EMAIL_PASS;
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;

      //Recipients
      $mail->setFrom(EMAIL_ADDRESS, 'Stop the Minerals Forum');
      $mail->addAddress($email, $name);

      $mail->isHTML(true);
      $mail->Subject = 'Together we can shut down the Coal Conference - Thanks for signing up';

      $body = 'Kia ora '.$name.',<br/><br/>Thank you for signing up to this historic event!<br/><br/>We are a diverse coalition of people who are dedicated to standing up against the 2019 NZ Minerals Forum (https://www.mineralsforum.co.nz/home); and standing up for our future. Despite unequivocal evidence that these industries should have no place in our future, the 2019 NZ Minerals Forum is attempting to meet to promote coal, and other polluting industries, expansion in Aotearoa NZ.<br/><br/>They did not expect the Coalition!<br/><bWe are committed to organising a strong, safe, inclusive, and non-violent event in opposition to the 2019 NZ Minerals Forum on 28th May, and are looking forward to having you on-board.<br/><br/>Please be in touch via this email address if you would like to discuss anything with us or get more involved. We are interested in hearing from anyone who can provide/is looking for car-pooling to and from the event; would like to volunteer during the lead up to the event; can share specific skills or resources with the Coalition; or is looking for/can provide accommodation in Dunedin. We are happy to talk with you.<br/><br/>Key events to be aware of are:<br/><a href="https://www.facebook.com/events/2261602390588816/">Blockade Information Evening, Thursday, 2 May, 6pm</a><br/>Pre-Blockade Briefing, Monday, 27 May, 6pm<br/><a href="https://www.facebook.com/events/333099857390071/">Stop the Minerals Forum Blockade, 28 May, all day</a><br/><br/>There will be more details and other events happening in the lead up to the event so please keep up to date by joining our <a href="https://www.facebook.com/StopTheMineralsForum/?fref=pb&hc_location=profile_browser">Facebook page</a>.<br/><br/>This is an exciting time to join the chorus of voices around the world that are saying "enough is enough" to fossil fuel industries. Now is our time for a victory.<br/><br/>Ng&amacr; mihi,<br/>The Coalition ';
      $mail->Body    = $body;
      $mail->send();
      return true;
    } catch (Exception $e) {
      //return $mail->ErrorInfo;
      return false;
    }
  }

  Flight::route('POST /register', function() {
    global $mysqli;
    $request = Flight::request();
    $form_data = $request->data;

    $search = [';', '%', '*', "'", '"'];
    $replace = ['\;', '\%', '\*', "\'", '\"'];
    $name   = str_replace($search, $replace, $form_data->name);
    $email  = str_replace($search, $replace, $form_data->email);
    $phone  = str_replace($search, $replace, $form_data->phone);
    $region = str_replace($search, $replace, $form_data->region);
    $accom  = str_replace($search, $replace, $form_data->accom);
    $trans  = str_replace($search, $replace, $form_data->trans);

    $query = "INSERT INTO registers (name, email, phone, region, accommodation, transport) VALUES ('".$name."', '".$email."', '".$phone."', '".$region."', '".$accom."', '".$trans."');";

    $res = mysqli_query($mysqli, $query);

    if ($res) {
      $e_res = sendEmail($name, $email);
      if ($e_res === true) {
        Flight::redirect('/registered', 402);
      } else {
        Flight::redirect('/error', 401);
      }
    } else {
      Flight::redirect('/error', 401);
    }
  });

  Flight::route('/resources', function () {
    Flight::render('header', array('page' => 'resources'), 'header_content');
    Flight::render('resources', array(), 'body_content');
    Flight::render('layout', array('title' => 'Resources'));
  });

  Flight::start();

?>
