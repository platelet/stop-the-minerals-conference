Kia ora friend,<br/><br/>

Thank you for signing up to this historic event!<br/><br/>

We are a diverse coalition of people who are dedicated to standing up against the 2019 NZ Minerals Forum (https://www.mineralsforum.co.nz/home); and standing up for our future. Despite unequivocal evidence that these industries should have no place in our future, the 2019 NZ Minerals Forum is attempting to meet to promote coal, and other polluting industries, expansion in Aotearoa NZ.<br/><br/>

They did not expect the Coalition!<br/><br/>

We are committed to organising a strong, safe, inclusive, and non-violent event in opposition to the 2019 NZ Minerals Forum on 28th May, and are looking forward to having you on-board.<br/><br/>

Please be in touch via this email address if you would like to discuss anything with us or get more involved. We are interested in hearing from anyone who can provide/is looking for car-pooling to and from the event; would like to volunteer during the lead up to the event; can share specific skills or resources with the Coalition; or is looking for/can provide accommodation in Dunedin. We are happy to talk with you.<br/><br/>

Key events to be aware of are:<br/>
<a href="https://www.facebook.com/events/2261602390588816/">Blockade Information Evening, Thursday, 2 May, 6pm</a><br/>
Pre-Blockade Briefing, Monday, 27 May, 6pm<br/>
<a href="https://www.facebook.com/events/333099857390071/">Stop the Minerals Forum Blockade, 28 May, all day</a><br/><br/>

There will be more details and other events happening in the lead up to the event so please keep up to date by joining our <a href="https://www.facebook.com/StopTheMineralsForum/?fref=pb&hc_location=profile_browser">Facebook page</a>.<br/><br/>

This is an exciting time to join the chorus of voices around the world that are saying “enough is enough” to fossil fuel industries. Now is our time for a victory.<br/><br/>

Ngā mihi,<br/>
The Coalition
